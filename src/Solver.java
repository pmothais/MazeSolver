import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Solver {
	
	private int width, height, startX, startY, endX, endY, currentX, currentY;
	private int maze[][];
	
	private final int UNDISCOVERED_BOX = 0;
	private final int WALL = 1;
	private final int DISCOVERED_BOX = 2;
	private final int INVALID_BOX = 3;
	private final int START = 4;
	private final int END = 5;
	
	/**
	 * Initialize the maze with input data
	 */
	public void init()
	{
		try
		{
			int i, j;
			
			File file = new File("input.txt");
			Scanner scanner = new Scanner(file);
			
			this.width = scanner.nextInt();
			this.height = scanner.nextInt();
			
			this.startX = this.currentX = scanner.nextInt();
			this.startY = this.currentY = scanner.nextInt();
			
			this.endX = scanner.nextInt();
			this.endY = scanner.nextInt();
			
			maze = new int[this.height][this.width];
						
			for (i = 0; i < height; i++) {
				for (j = 0; j < width; j++) {
					maze[i][j] = scanner.nextInt();
				}
			}
			
			scanner.close();
		}
		catch (FileNotFoundException exception)
		{
			System.out.println("The file was not found");
		}
	}
	
	/**
	 * Solves the maze
	 */
	public void solve()
	{
		boolean complete = false;
		
		while (complete == false)
		{	
			// I am trying to move to an undiscovered box
			// If I can, I do it, else I rollback
			if (!nextStep(UNDISCOVERED_BOX)) {
				if (!nextStep(DISCOVERED_BOX)) {
					complete = true;
					System.out.println("This maze has not solution");
				}
			}
			
			// If the current position is equals to the end position, job done
			if (this.currentX == this.endX && this.currentY == this.endY) {
				complete = true;
			}
		}
	}
	
	/**
	 * Do a movement according to the state of the next box
	 * @param state
	 * @return
	 */
	private boolean nextStep(int state)
	{
		boolean res = false;
		
		// I check which movement I can do and I do the first possible
		if (canEMovement(state)) {
			doEMovement(state);
			res = true;
		} else if (canSMovement(state)) {
			doSMovement(state);
			res = true;
		} else if (canWMovement(state)) {
			doWMovement(state);
			res = true;
		} else if (canNMovement(state)) {
			doNMovement(state);
			res = true;
		}
		
		return res;
	}
	
	/**
	 * Check if a E movement is possible
	 * @param state
	 * @return
	 */
	private boolean canEMovement(int state)
	{
		return (this.maze[this.currentY][this.currentX + 1] == state) ? true : false;
	}
	
	/**
	 * Check if a S movement is possible
	 * @param state
	 * @return
	 */
	private boolean canSMovement(int state)
	{
		return (this.maze[this.currentY + 1][this.currentX] == state) ? true : false;
	}
	
	/**
	 * Check if a W movement is possible
	 * @param state
	 * @return
	 */
	private boolean canWMovement(int state)
	{
		return (this.maze[this.currentY][this.currentX - 1] == state) ? true : false; 
	}
	
	/**
	 * Check if a N movement is possible
	 * @param state
	 * @return
	 */
	private boolean canNMovement(int state)
	{
		return (this.maze[this.currentY - 1][this.currentX] == state) ? true : false;
	}
	
	/**
	 * Do a E movement
	 * @param state
	 */
	private void doEMovement(int state)
	{		
		setStateBox(state);
		this.currentX++;
	}
	
	/**
	 * Do a S movement
	 * @param state
	 */
	private void doSMovement(int state)
	{		
		setStateBox(state);		
		this.currentY++;
	}
	
	/**
	 * Do a W movement
	 * @param state
	 */
	private void doWMovement(int state)
	{
		setStateBox(state);
		this.currentX--;
	}
	
	/**
	 * Do a N movement
	 * @param state
	 */
	private void doNMovement(int state)
	{
		setStateBox(state);
		this.currentY--;
	}
	
	/**
	 * Set the state of the current box
	 * @param state
	 */
	private void setStateBox(int state)
	{
		if (state == UNDISCOVERED_BOX) {
			markBox();
		} else if (state == DISCOVERED_BOX){
			unmarkBox();
		}
	}
	
	/**
	 * Mark the current box because it is a possible way
	 */
	private void markBox()
	{
		this.maze[this.currentY][this.currentX] = DISCOVERED_BOX;
	}
	
	/**
	 * Unmark the current box because it is wrong way
	 */
	private void unmarkBox()
	{
		this.maze[this.currentY][this.currentX] = INVALID_BOX;
	}
	
	/**
	 * Print the solution on the console to show it at the user
	 */
	public void showSolution()
	{
		int i, j;
		
		this.maze[this.startY][this.startX] = START;
		this.maze[this.endY][this.endX] = END;
		
		for (i = 0; i < this.height; i++) {
			for (j = 0; j < this.width; j++) {
				switch (this.maze[i][j]) {
					case UNDISCOVERED_BOX:
						System.out.print(" "); 
						break;
					case WALL:
						System.out.print("#");
						break;
					case INVALID_BOX:
						System.out.print(" ");
						break;
					case DISCOVERED_BOX:
						System.out.print("X");
						break;
					case START:
						System.out.print("S");
						break;
					case END:
						System.out.print("E");
						break;	
				}
			}
			System.out.println();
		}
	}
}
