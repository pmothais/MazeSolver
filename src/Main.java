public class Main {

	public static void main(String[] args) {
		
		Solver solver = new Solver();
		
		try {
			solver.init();
			solver.solve();
			solver.showSolution();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
