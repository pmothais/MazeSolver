
# Maze Solver

This is a simple maze solver

## Process to run the maze solver

1. Compile java files
	* Open your command line interface
	* Go to the project directory containing the java files
	* Use the command line : "javac Main.java Solver.java". Executable files are created
2. Put a valid input file at the root of the project. The file must be named "input.txt"
3. Run the executable file Main with the command line : "java Main"

## Maze file format

The input is a maze description file in plain text.  
 1 - denotes walls
 0 - traversable passage way

INPUT :

	<WIDTH> <HEIGHT><CR>
	<START_X> <START_Y><CR>		// (x,y) location of the start. (0,0) is upper left and (width-1,height-1) is lower right
	<END_X> <END_Y><CR>		// (x,y) location of the end
	<HEIGHT>			// rows where each row has <WIDTH> {0,1} integers space delimited

OUTPUT :

 The maze with a path from start to end.
 * Walls are marked by '#'
 * Passages are marked by ' '
 * Path is marked by 'X'
 * Start/End points are marked by 'S'/'E'

## Example

INPUT :

	10 10  
	1 1  
	8 8  
	1 1 1 1 1 1 1 1 1 1  
	1 0 0 0 0 0 0 0 0 1  
	1 0 1 0 1 1 1 1 1 1  
	1 0 1 0 0 0 0 0 0 1  
	1 0 1 1 0 1 0 1 1 1  
	1 0 1 0 0 1 0 1 0 1  
	1 0 1 0 0 0 0 0 0 1  
	1 0 1 1 1 0 1 1 1 1  
	1 0 1 0 0 0 0 0 0 1  
	1 1 1 1 1 1 1 1 1 1  


OUTPUT:

	##########
	#SXX     #
	# #X######
	# #XX    #
	# ##X# ###
	# # X# # #
	# # XX   #
	# ###X####
	# #  XXXE#
	##########